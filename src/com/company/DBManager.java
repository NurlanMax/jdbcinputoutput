package com.company;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class DBManager {
    private Connection connection;

    public void connect() {
        try {
//            Class.forName("org.postgresql.Driver");
//            String url = "jdbc:postgresql://localhost:5432/contactdb";
//            String login = "postgres";
//            String password = "megatiger1998";
//            Connection connection  = DriverManager.getConnection(url, login, password);
////            Connection connection = DriverManager.getConnection(url, login, password);
            connection = DriverManager.getConnection(
                    "jdbc:postgresql://localhost:5432/contactdb?useUnicode=true&serverTimezone=UTC", "postgres", "megatiger1998"
            );
            System.out.println("Connected");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public ArrayList<Client> getAllClient() {
        ArrayList<Client> clients = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement("SELECT * FROM JC_CONTACT");
            ResultSet rs = st.executeQuery();


            while (rs.next()) {
                Long id = rs.getLong("contact_id");
                String firstName = rs.getString("first_name");
                String lastName = rs.getString("last_name");
                String phoneNum = rs.getString("phone");
                String email=rs.getString("email");
                clients.add(new Client(id, firstName, lastName, phoneNum,email));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return clients;
    }
    public void addClient(Client client) {
        try {
            PreparedStatement st= connection.prepareStatement
                    ("INSERT INTO JC_CONTACT(first_name,last_name,phone,email) values(?,?,?,?)");
            st.setString(1,client.getFirstName());
            st.setString(2,client.getLastName());
            st.setString(3,client.getPhoneNum());
            st.setString(4,client.getEmail());
            st.executeUpdate();
            st.close();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

}
